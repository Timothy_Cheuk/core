import re
import time
import ujson
import sys
from os import error

class InvalidFunctionError(Exception):
    pass

def filterByText(textDict,regular_expression):
    resultDict = {}
    for k,v in textDict["dictionary"].items():
        if re.search(regular_expression,v["text"]) is not None:
            resultDict[k] = v
    return resultDict

def filterByAttributesRe(textDict,attribute,regular_expression):
    resultDict = {}
    for k,v in textDict["dictionary"].items():
        if attribute in v["attributes"]:
            if type(v["attributes"][attribute]) is str:
                if re.search(regular_expression,v["attributes"][attribute]) is not None:
                    resultDict[k] = v
    return resultDict

def filterByAttributesFunction(textDict,function):
    
    #check if the arguments is a function
    try:
        if not callable(function):
            raise InvalidFunctionError
    except error:
        return error

    resultDict = {}
    for k,v in textDict["dictionary"].items():
        for k_a,v_a in v["attributes"].items():
                result = function(v_a)

                #check if return is a boolean
                try:
                    if type(result) is not bool:
                        raise InvalidFunctionError
                except error:
                    return error

                if function(v_a):
                    resultDict[k] = v
                
    return resultDict

# start_time = time.time()
# JSONString = open("./sandbox/testdata/test.json", "r").read()
# dictdata = r.readDictFromJsonString(JSONString)
# # resultdata = filterByText(dictdata,"Meeting")
# resultdata = filterByAttributesRe(dictdata,"page_no","0")
# print(resultdata)
# print("%s seconds for filtering" % (time.time() - start_time))