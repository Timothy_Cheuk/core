import ujson
from utilfunction import *
from os import error

def readDictFromJsonString(JSONString):
  try:
    JSONDict = ujson.loads(JSONString)
    if not validateDict(JSONDict):
      raise InvalidFormatError
    return JSONDict
  except error:
    return error

def readDictFromCSVString(CSVString,delimeter = "\t"):
    return

