from os import error
import ujson

class InvalidFormatError(Exception):
    pass

def validateDict(JSONDict:dict):
  #check if there is a dictionary in the dict
  if "dictionary" in JSONDict:
    return True
  else:
    return False
  return True

def loadJSONStringToDict(JSONString):
  try:
    JSONDict = ujson.loads(JSONString)
    if not validateDict(JSONDict):
      raise InvalidFormatError
    return JSONDict
  except error:
    return error

