import re
import time
import ujson
import sys
import read as r
from filter import *
from appendattributes import *
from os import error

start_time = time.time()
JSONString = open("./sandbox/testdata/test.json", "r").read()
dictdata = r.readDictFromJsonString(JSONString)
# resultdata = filterByText(dictdata,"Meeting")
resultdata = filterByText(dictdata,"meeting")
# print(resultdata.keys())
appendattributes(resultdata,resultdata.keys(),"label","meeting")
print(len(resultdata))
print("%s seconds for appending" % (time.time() - start_time))