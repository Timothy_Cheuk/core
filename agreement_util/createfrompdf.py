from os import error
import fitz
import numpy as np
import ujson
import time
import csv
import io

# print(np.__version__)
# print(fitz.__doc__)
# start_time = time.time()
# doc = fitz.open("C:/Users/Tim/Downloads/display/aapl.pdf")
# text = np.asarray([(*a,"0") for a in doc[0].get_textpage().extractWORDS()])

# for page in doc.pages(1,doc.page_count,1):
#     try:
#         new_a = np.asarray([(*a,str(page.number)) for a in page.get_textpage().extractWORDS()])
#         if (new_a.size > 0):
#             text = np.concatenate((text,new_a))
#     except error:
#         print(page.number)
#         print("have error")
#         print(error)
# ids = np.arange(text.size)
# textdict= {"dictionary":{A:{"text":B[4],"attributes":{"bbox":B[0:4].tolist(),"block_no":B[5],"line_no":B[6],"word_no":B[7],"page_no":B[8]}} for A, B in zip(ids, text)}}

# textjson = ujson.dumps(textdict)
# with open('./sandbox/testdata/test.json', 'w') as f:
#     f.write(ujson.dumps(textdict))

def createDictFromPDF(path,toDisk=False):
    start_time = time.time()
    doc = fitz.open(path)
    text = np.asarray([(*a,"0") for a in doc[0].get_textpage().extractWORDS()])
    for page in doc.pages(1,doc.page_count,1):
        try:
            new_a = np.asarray([(*a,str(page.number)) for a in page.get_textpage().extractWORDS()])
            if (new_a.size > 0):
                text = np.concatenate((text,new_a))
        except error:
            print(page.number)
            print("have error")
            print(error)
    ids = np.arange(text.size)
    textdict= {"dictionary":{A:{"text":B[4],"attributes":{"bbox":B[0:4].tolist(),"block_no":B[5],"line_no":B[6],"word_no":B[7],"page_no":B[8]}} for A, B in zip(ids, text)}}
    print("%s seconds for reading pdf" % (time.time() - start_time))
    textdict["analytics"] = ""
    return textdict

def createJsonStringFromPdf(path,toDisk=False):
    return ujson.dumps(createDictFromPDF(path,toDisk))

def createCSVStringFromPDF(path,delimeter=None,toDisk=False):

    #default value for delimeter
    if delimeter is None:
        delimeter = '\t'

    textdict = createDictFromPDF(path,toDisk)

    output = io.StringIO()
    fields=["key","value"]
    writer = csv.writer(output, delimiter='\t')
    writer.writerow(fields)

    for key,value in textdict.items():
        # print(key)
        if key != "dictionary":
            # verify is this correct
            if type(value) is dict or type(value) is list:
                writer.writerow({key:ujson.dumps(value)})
            else:
                writer.writerow([key,value])

    writer.writerow(["id","text","attributes"])
    for key,value in textdict["dictionary"].items():
        writer.writerow([key,value["text"],ujson.dumps(value["attributes"])])

    return output.getvalue()

# start_time = time.time()
# text = createCSVStringFromPDF("C:/Users/Tim/Downloads/display/aapl.pdf")
# with open('./sandbox/testdata/test.csv', 'w',errors='replace') as f:
#     f.write(text)
# print("%s seconds for generating csv" % (time.time() - start_time))

# start_time = time.time()
# text = createJsonStringFromPdf("C:/Users/Tim/Downloads/display/aapl.pdf")
# with open('./sandbox/testdata/test.json', 'w',errors='replace') as f:
#     f.write(text)
# print("%s seconds for generating json" % (time.time() - start_time))