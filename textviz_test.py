import textviz as tv
import agreement_util.createfrompdf as create
import agreement_util.filter as filter
import agreement_util.appendattributes as append
import ujson as uj

file_path = "D:/BOS_internship/Code/Core/core/tsla.pdf"
dest_path_0 = "D:\BOS_internship\Code\Core\core"
dest_path_1 = "D:/BOS_internship/Code/Core/core"
exe_path = "D:/BOS_internship/Code/Core/core/pdf2htmlEX"

def textviz_entry(exe,dest,file,dictdata):
    tv.pdf2html(exe, dest, file)
    html = tv.readhtml(dest_path_1,"tsla.html")
    labels = filter.filterByAttributesRe(dictdata,"label","important")
    for k,v in labels.items():
        coord = v["attributes"]["bbox"]
        html = tv.label(html,int(v["attributes"]["page_no"])+1,float(coord[0]),float(coord[1]),float(coord[2]),float(coord[3]),"purple")
    tv.writehtml(html,dest_path_1+"/test.html")
    return


#simulate the text extraction process
dictdata= create.createDictFromPDF(file_path)
# with open('./sandbox/testdata/test.json', 'w',errors='replace') as f:
#     f.write(uj.dumps(dictdata))

#simulate labelling process
key_information = filter.filterByText(dictdata,"Meeting")
print(list(key_information.keys()))
append.appendattributes(dictdata,key_information.keys(),"label","important")
with open('./sandbox/testdata/test_labelled.json', 'w',errors='replace') as f:
    f.write(uj.dumps(dictdata))

#label obtained, visualize the label
textviz_entry(exe_path,dest_path_0,file_path,dictdata)