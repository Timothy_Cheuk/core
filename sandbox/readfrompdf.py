from os import error
import fitz
import numpy as np
import ujson
import time
print(np.__version__)
print(fitz.__doc__)
start_time = time.time()
doc = fitz.open("C:/Users/Tim/Downloads/display/aapl.pdf")
# text = (doc[0].get_textpage().extractWORDS())
# text = np.asarray(doc[0].get_textpage().extractWORDS())
text = np.asarray([(*a,"0") for a in doc[0].get_textpage().extractWORDS()])
# print(text)
# print(text.size)
for page in doc.pages(1,doc.page_count,1):
    try:
        new_a = np.asarray([(*a,str(page.number)) for a in page.get_textpage().extractWORDS()])
        if (new_a.size > 0):
            text = np.concatenate((text,new_a))
    except error:
        print(page.number)
        print("have error")
        print(error)
ids = np.arange(text.size)
# print(text.size)
# print(ids.size)
# print(ids)
textdict= {"dictionary":{A:{"text":B[4],"attributes":{"bbox":B[0:4].tolist(),"block_no":B[5],"line_no":B[6],"word_no":B[7],"page_no":B[8]}} for A, B in zip(ids, text)}}

# print(len(textdict.get("dictionary")))
textjson = ujson.dumps(textdict)
with open('./sandbox/testdata/test.json', 'w') as f:
    f.write(ujson.dumps(textdict))

print("--- %s seconds ---" % (time.time() - start_time))